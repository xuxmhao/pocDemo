package my.poc.demo.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hm.poc.IPocEngineEventHandler;
import com.hm.poc.PocEngineFactory;
import com.hm.poc.greendao.Channel;
import com.unionbroad.app.PocApplication;
import com.unionbroad.app.util.Logger;

import java.util.ArrayList;
import java.util.List;

import my.poc.demo.R;
import my.poc.demo.activity.AvActivity;
import my.poc.demo.widget.InterceptViewPager;


/**
 * 频道类型: Channel.getChannelType()
 * "4"：语音对讲组
 * <p>
 * ----------------其它类型暂时不支持------------------
 * "1"：视频对讲
 * "2"：语音会议
 * "5"：视频会议
 */
public class ChannelListFragment extends Fragment {

    private Logger logger = Logger.getLogger("ChannelListFragment");
    private MyAdapter adapter;
    private ArrayList<Channel> channels = new ArrayList<>();
    private RecyclerView recyclerView;
    private InterceptViewPager homeViewPager;

    public ChannelListFragment() {
    }

    public static ChannelListFragment newInstance() {
        return new ChannelListFragment();
    }

    public void bindViewPager(InterceptViewPager viewPager) {
        homeViewPager = viewPager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (recyclerView == null) {
            recyclerView = (RecyclerView) inflater.inflate(R.layout.home_fragment_channellist, container, false);

            channels.clear();
            channels.addAll(PocEngineFactory.get().getChannelList());
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new MyAdapter();
            recyclerView.setAdapter(adapter);
        }
        if (recyclerView.getParent() != null) {
            ((ViewGroup) recyclerView.getParent()).removeView(recyclerView);
        }
        return recyclerView;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView type;


        MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            type = (TextView) itemView.findViewById(R.id.type);
        }
    }

    class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new MyViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_channel, null));
        }

        @Override
        public void onBindViewHolder(MyViewHolder viewHolder, final int position) {
            final Channel channel = channels.get(position);
            viewHolder.name.setText(channel.getChannelName() + ": " + channel.getChannelNumber());
            viewHolder.type.setText(channel.getChannelType());
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final List<String> items = new ArrayList<>();
                    if (TextUtils.equals(PocApplication.sUserNumber + "", channel.getCreateType())) {
                        items.add("删除群组");
                    }
                    if (IPocEngineEventHandler.ChannelType.AUDIO_PTT.equals(channel.getChannelType())) {
                        items.add("进入群组");
                    } else if (IPocEngineEventHandler.ChannelType.VIDEO_MEETING.equals(channel.getChannelType())) {
                        items.add("进入视频会议");
                    } else if (IPocEngineEventHandler.ChannelType.AUDIO_MEETING.equals(channel.getChannelType())) {
                        items.add("进入语音会议");
                    }
                    new AlertDialog.Builder(v.getContext()).setItems(items.toArray(new String[items.size()]),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if ("进入群组".equals(items.get(i))) {
                                        PocEngineFactory.get().joinChannel(channel.getChannelNumber() + "");
                                        if (homeViewPager != null) {
                                            homeViewPager.setCurrentItem(0);
                                        }
                                    } else if ("删除群组".equals(items.get(i))) {
                                        PocEngineFactory.get().removeAudioPTTChannel(channel.getChannelNumber(), new IPocEngineEventHandler.Callback<Boolean>() {
                                            @Override
                                            public void onResponse(Boolean aBoolean) {
                                                if (aBoolean != null && aBoolean) {
                                                    Toast.makeText(getContext(), "删除成功", Toast.LENGTH_SHORT).show();
                                                    channels.remove(channel);
                                                    adapter.notifyDataSetChanged();
                                                } else {
                                                    Toast.makeText(getContext(), "删除失败", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    } else if ("进入视频会议".equals(items.get(i))) {
                                        Intent intent = new Intent(getContext().getApplicationContext(), AvActivity.class);
                                        intent.putExtra("callerId", channel.getChannelNumber() + "");
                                        intent.putExtra("type", IPocEngineEventHandler.SessionType.TYPE_VIDEO_MEETING);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        getContext().getApplicationContext().startActivity(intent);
                                    } else if ("进入语音会议".equals(items.get(i))) {
                                        Intent intent = new Intent(getContext().getApplicationContext(), AvActivity.class);
                                        intent.putExtra("callerId", channel.getChannelNumber() + "");
                                        intent.putExtra("type", IPocEngineEventHandler.SessionType.TYPE_AUDIO_MEETING);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        getContext().getApplicationContext().startActivity(intent);
                                    }
                                }
                            }).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return channels.size();
        }
    }
}
