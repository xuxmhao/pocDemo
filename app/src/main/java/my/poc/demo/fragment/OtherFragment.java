package my.poc.demo.fragment;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.hm.poc.IPocEngineEventHandler;
import com.hm.poc.PocEngine;
import com.hm.poc.PocEngineFactory;
import com.hm.poc.RuntimeInfo;
import com.hm.poc.chat.ChatMessageStatus;
import com.hm.poc.crash.HmCrashReporter;
import com.hm.poc.globe.GlobeHolderKt;
import com.hm.poc.greendao.Channel;
import com.hm.poc.greendao.ChatMessage;
import com.hm.poc.greendao.Department;
import com.hm.poc.greendao.MessageDialogue;
import com.hm.poc.greendao.User;
import com.hm.poc.http.data.AppVersionReq;
import com.hm.poc.http.data.LocationHistoryResponse;
import com.hm.poc.log.ALog;
import com.hm.poc.upgrade.UpgradeService;
import com.hm.poc.utils.CommonUtilKt;
import com.unionbroad.app.holder.CurrentChannelHolder;
import com.unionbroad.app.manager.TTSManager;
import com.unionbroad.app.util.Logger;

import java.util.List;

import my.poc.demo.R;
import my.poc.demo.activity.AvActivity;
import my.poc.demo.activity.ChatMessageActivity;
import my.poc.demo.activity.MapActivity;
import my.poc.demo.cc.CCHelper;
import my.poc.demo.widget.MultipleSelectDialog;


public class OtherFragment extends Fragment {

    private Logger logger = Logger.getLogger("OtherFragment");
    private View rootView;
    private TextView console;
    private LinearLayout funcContentLayout;
    private PocEngine pocEngine;
    private Handler handler = new Handler();

    public OtherFragment() {
    }

    public static OtherFragment newInstance() {
        return new OtherFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.home_fragment_other, container, false);
            initView();
        }
        if (rootView.getParent() != null) {
            ((ViewGroup) rootView.getParent()).removeView(rootView);
        }
        return rootView;
    }

    private void initView() {
        pocEngine = PocEngineFactory.get();

        console = (TextView) rootView.findViewById(R.id.console);
        funcContentLayout = (LinearLayout) rootView.findViewById(R.id.func_content_layout);
        testUpgrade();
        showAudioMeetingAndMonitor();
        getContactsUserList();
        showQueryMeeting();
        showAudioSourceSwitch();
        showSpeakPhoneSwitch();
        showFeedback();
        showCurrentChannelUsers();
        showDepartments();
        showDispatchUsers();
        showSDKVersionInfo();
        showLastLocationReportInfo();
        showTTSFunc();
        showChannelListenFunc();
        showAllListenChannelIds();
        showCancelAllListenChannels();

        addAlarmFunc();
        addGlobalPttFunc();
        addUserGPSFunc();
        addGPSTrackListFunc();
        addReportLocationFunc();
        addGetInChannelUsersFunc();
        addCreateMessageDialogueIfNeedFunc();
        addRefreshUserStatusFunc();
        addChangePasswordFunc();
        addVolumeUpFunc();
        addVolumeDownFunc();
        addVolumeSetFunc();
        addGetMaxVolumeLevelFunc();
        addMapViewFunc();
        showMessageUnReadFunc();
        showSetMessageAsReadFunc();
        showCrashFunc();
    }

    private void testUpgrade() {
        Button button = new Button(getContext());
        button.setText("检测升级");
        funcContentLayout.addView(button);
        button.setOnClickListener(v -> {
            UpgradeService.INSTANCE.checkUpgrade(new UpgradeService.CheckParam(
                    new AppVersionReq(RuntimeInfo.INSTANCE.getEnterpriseCode(), "pocDemo", CommonUtilKt.curUniqueCode()),
                    true, getContext(), null, null, true
            ));
        });
    }

    private void showAudioMeetingAndMonitor() {
        Button button = new Button(getContext());
        button.setText("主动发起会诊");
        funcContentLayout.addView(button);
        button.setOnClickListener(v -> {
            MultipleSelectDialog dialog = new MultipleSelectDialog(getContext(), pocEngine.getContactsUserList());
            dialog.setCustomTitle("选择参会人");
            dialog.setOnClickOkListener(() -> {
                List<User> selects = dialog.getSelects();
                if (selects.size() <= 1) {
                    GlobeHolderKt.getToastService().showToast("人数不够");
                    return;
                }
                button.setEnabled(false);
                CCHelper.INSTANCE.requestConsultation(selects, result -> {
                    Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
                    button.setEnabled(true);
                });
            });
            dialog.show();
        });
    }

    private void getContactsUserList() {
        Button button = new Button(getContext());
        button.setText("获取联系人信息");
        funcContentLayout.addView(button);
        button.setOnClickListener(v -> {
            List<User> contactsUserList = PocEngineFactory.get().getContactsUserList();
            for (User user : contactsUserList) {
                logger.i("user= " + user);
            }
        });
    }

    private void showQueryMeeting() {
        Button button = new Button(getContext());
        button.setText("查询会议信息");
        funcContentLayout.addView(button);
        button.setOnClickListener(v -> {
            for (Channel channel : PocEngineFactory.get().getChannelList()) {
                long remoteId = channel.getChannelNumber();
                PocEngineFactory.get().queryMeetingConfig(remoteId, result -> {
                    logger.i("queryMeetingConfig.onResponse: " + result);
                    console.post(() -> console.setText(result.toString()));
                });
            }
        });
    }

    private void showAudioSourceSwitch() {
        Button button = new Button(getContext());
        button.setText("切换mic");
        funcContentLayout.addView(button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag() != null) {
                    v.setTag(null);
                    pocEngine.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
                } else {
                    v.setTag("hh");
                    pocEngine.setAudioSource(MediaRecorder.AudioSource.MIC);
                }
            }
        });
    }

    private void showSpeakPhoneSwitch() {
        Button button = new Button(getContext());
        button.setText("打开或关闭免提");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean enable = pocEngine.isSpeakerphoneEnable();
                pocEngine.setEnableSpeakerphone(!enable);
                Toast.makeText(getContext(), "setEnableSpeakerphone=" + (!enable), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showFeedback() {
        Button button = new Button(getContext());
        button.setText("反馈log");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pocEngine.autoFeedback("poc-sdk日志反馈: " + System.currentTimeMillis());
            }
        });
    }

    private void showCurrentChannelUsers() {
        Button button = new Button(getContext());
        button.setText("获取当前组成员");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Channel channel = CurrentChannelHolder.getInstance().getCurrentChannel();
                List<User> list = channel.getUsers();
                StringBuilder sb = new StringBuilder();
                for (User user : list) {
                    sb.append(user.getName()).append(",");
                }
                console.setText(sb.toString());
            }
        });
    }

    private void showDepartments() {
        Button button = new Button(getContext());
        button.setText("获取部门架构");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Department> list = pocEngine.getDepartments();
                StringBuilder sb = new StringBuilder();
                for (Department user : list) {
                    sb.append(user.getName()).append(",");
                }
                console.setText(sb.toString());
            }
        });
    }

    private void showDispatchUsers() {
        Button button = new Button(getContext());
        button.setText("所有调度台账号");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<User> list = pocEngine.getDispatcherUserList();
                StringBuilder sb = new StringBuilder();
                for (User user : list) {
                    sb.append(user.getNumber()).append(",");
                }
                console.setText(sb.toString());
            }
        });
    }

    private void showSDKVersionInfo() {
        Button button = new Button(getContext());
        button.setText("显示SDK版本号");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                console.setText(pocEngine.getSDKVersion());
            }
        });
    }

    private void showLastLocationReportInfo() {
        Button button = new Button(getContext());
        button.setText("显示最新一次位置上报内容");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IPocEngineEventHandler.LocationReportInfo info =
                    pocEngine.getLastLocationReportInfo();
                StringBuilder sb = new StringBuilder();
                sb.append("jd=" + info.jd + " wd=" + info.wd + " time=" + info.time);
                console.setText(sb.toString());
            }
        });
    }

    private void showTTSFunc() {
        Button button = new Button(getContext());
        button.setText("调用TTS");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ALog.d("TTSManager", "onclick");
                TTSManager.getInstance().speaking("你好，我来自 SDK", TTSManager.MODE_NORMAL);
            }
        });
    }

    private void showCancelAllListenChannels() {
        Button button = new Button(getContext());
        button.setText("取消正在监听的频道");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Channel> channels = pocEngine.getListeningChannels();
                int delayTime = 500;
                for (final Channel channel : channels) {
                    funcContentLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //监听和取消监听都是一个轻度阻塞的过程，建议不要同时操作多个
                            pocEngine.cancelListenChannel(channel.getChannelNumber() + "");
                        }
                    }, delayTime);
                    delayTime = +delayTime;
                }
            }
        });


    }

    private void showAllListenChannelIds() {
        Button button = new Button(getContext());
        button.setText("显示正在监听的频道");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Channel> channels = pocEngine.getListeningChannels();
                StringBuilder sb = new StringBuilder();
                for (Channel channel : channels) {
                    sb.append(channel.getChannelName() + " - ");
                }
                console.setText(sb.toString());
            }
        });
    }

    private void showChannelListenFunc() {
        Button button = new Button(getContext());
        button.setText("监听一个频道");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Channel> channels = pocEngine.getChannelList();
                if (channels.size() > 0) {
                    boolean ret = pocEngine.listenChannel(channels.get(0).getChannelNumber() + "");
                    //已经进入的群组，再调用监听会失败
                    if (!ret) {
                        Toast.makeText(getActivity(), "监听失败", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void showCrashFunc() {
        Button button = new Button(getContext());
        button.setText("主动引发异常，测试进程重启");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HmCrashReporter.INSTANCE.testCrash();
            }
        });
    }

    private void showSetMessageAsReadFunc() {
        Button button = new Button(getContext());
        button.setText("设置一条未读消息为已读");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatMessage unReadMessage = null;
                List<MessageDialogue> dialogues = pocEngine.getAllConversation();
                for (MessageDialogue dialogue : dialogues) {
                    List<ChatMessage> messages = pocEngine.getConversationMessages(dialogue.getChat_id());
                    for (ChatMessage message : messages) {
                        if (message.getMsg_status() == ChatMessageStatus.Chat.UNREAD) {
                            unReadMessage = message;
                            break;
                        }
                    }
                    if (unReadMessage != null) {
                        break;
                    }
                }

                if (unReadMessage != null) {
                    //设置已读
                    pocEngine.markMessageAsRead(unReadMessage);

                    //显示最新未读数量
                    int unRead = 0;
                    for (MessageDialogue dialogue : dialogues) {
                        unRead = unRead + dialogue.getUnread();
                    }
                    console.setText("最新未读消息数：" + unRead);
                } else {
                    Toast.makeText(getContext(), "当前没有未读消息", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showMessageUnReadFunc() {
        Button button = new Button(getContext());
        button.setText("未读消息数");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<MessageDialogue> dialogues = pocEngine.getAllConversation();
                int unRead = 0;
                for (MessageDialogue dialogue : dialogues) {
                    unRead = unRead + dialogue.getUnread();
                }
                console.setText("未读消息：" + unRead);
            }
        });
    }

    private void addMapViewFunc() {
        Button button = new Button(getContext());
        button.setText("显示地图控件");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.getContext().startActivity(new Intent(v.getContext(), MapActivity.class));
            }
        });
    }

    private void addGetMaxVolumeLevelFunc() {
        Button button = new Button(getContext());
        button.setText("获取当前声音通道最大档位");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("最大档位: " + pocEngine.getMaxVolumeLevel());
            }
        });
    }

    private void addVolumeSetFunc() {
        Button button = new Button(getContext());
        button.setText("设置音量到5档");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pocEngine.setCurrentStreamVolumeLevel(5, AudioManager.FLAG_SHOW_UI);
            }
        });
    }

    private void addVolumeUpFunc() {
        Button button = new Button(getContext());
        button.setText("当前声音通道，音量+");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pocEngine.volumeUp(AudioManager.FLAG_SHOW_UI);
            }
        });
    }

    private void addVolumeDownFunc() {
        Button button = new Button(getContext());
        button.setText("当前声音通道，音量-");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pocEngine.volumeDown(AudioManager.FLAG_SHOW_UI);
            }
        });
    }

    private void addChangePasswordFunc() {
        Button button = new Button(getContext());
        button.setText("修改密码");
        funcContentLayout.addView(button);

        //FIXME 修改密码接口，目前后台会一直返回错误，需要排查一下
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pocEngine.changePassword("123456", new IPocEngineEventHandler.Callback<Boolean>() {
                    @Override
                    public void onResponse(Boolean aBoolean) {
                        Toast.makeText(getContext(), aBoolean ? "修改成功" : "修改失败", Toast.LENGTH_SHORT).show();
                        if (aBoolean) {
                            //TODO 这里一般需要弹框提示重启，流程就是杀死自己进程，再定时2秒重启自己
                        }
                    }
                });
            }
        });
    }

    private void addAlarmFunc() {
        Button button = new Button(getContext());
        button.setText("一键报警并呼叫");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean ret = pocEngine.alarm();
                List<User> dispatcherUserList = pocEngine.getDispatcherUserList();
                Toast.makeText(getContext(), "报警:" + ret, Toast.LENGTH_SHORT).show();
                if (dispatcherUserList != null && dispatcherUserList.size() > 0) {
                    for (User user : dispatcherUserList) {
                        //逻辑就是哪个调度台在线，就呼叫谁（此处为了调试写死呼叫2008）
                        if (user.getStatus() == 1 && user.getNumber() == 2008) {
                            Intent intent = new Intent(getContext().getApplicationContext(), AvActivity.class);
                            intent.putExtra("callerId", user.getNumber() + "");
                            intent.putExtra("type", IPocEngineEventHandler.SessionType.TYPE_VIDEO_MONITOR_CALL);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getContext().startActivity(intent);
                            break;
                        }
                    }
                }
            }
        });
    }

    private void addGlobalPttFunc() {
        Button button = new Button(getContext());
        button.setText("显示or隐藏全局ptt按钮");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean show = "1".equals(v.getTag()) ? true : false;
                pocEngine.showGlobalPTTButton(!show);
                v.setTag(!show ? "1" : "0");
            }
        });
    }

    private void addUserGPSFunc() {
        Button button = new Button(getContext());
        button.setText("获取gps坐标");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("请求中");
                pocEngine.getUserGPS(pocEngine.getContactsUserList(), json -> console.setText(json.toString()));
            }
        });
    }

    private void addGPSTrackListFunc() {
        Button button = new Button(getContext());
        button.setText("单个用户gps轨迹");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("请求中");
                pocEngine.getGPSTrackList(pocEngine.getCurrentUser().getNumber() + "", "1970-01-01 00:00", "2020-01-01 00:00", new IPocEngineEventHandler.Callback<LocationHistoryResponse>() {
                    @Override
                    public void onResponse(final LocationHistoryResponse result) {
                        console.setText(result.toString());
                    }
                });
            }
        });
    }

    private void addReportLocationFunc() {
        Button button = new Button(getContext());
        button.setText("主动上报自己位置");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("上报中");
                final String longitude = "116.313946";
                final String latitude = "39.856953";
                pocEngine.reportLocationChange(longitude, latitude, "", "test addr", new IPocEngineEventHandler.Callback<Boolean>() {
                    @Override
                    public void onResponse(final Boolean aBoolean) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                console.setText("上报：[" + longitude + "," + latitude + "] " + aBoolean);
                            }
                        });
                    }
                });
            }
        });
    }

    private void addGetInChannelUsersFunc() {
        Button button = new Button(getContext());
        button.setText("当前对讲组中在线的人");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                console.setText("对讲组前台在线人： " + pocEngine.getInChannelUsers());
            }
        });
    }

    private void addCreateMessageDialogueIfNeedFunc() {
        Button button = new Button(getContext());
        button.setText("进入当前对讲组的IM界面");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialogue md = pocEngine.createMessageDialogueIfNeed(pocEngine.getCurrentPttChannel());
                ChatMessageActivity.show(getActivity(), md.getChat_id());
            }
        });
    }

    private void addRefreshUserStatusFunc() {
        Button button = new Button(getContext());
        button.setText("主动刷新联系人状态");
        funcContentLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pocEngine.requestUsersStatusImmediately();
            }
        });
    }
}