package my.poc.demo.cc;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * @author benzly
 * @date 2024/1/14
 */
public abstract class FilterZeroAdapter extends BaseAdapter {
    protected int lastPosition = -1;
    public View getView(int position, View convertView, ViewGroup parent){
        if(getCount()>1){
            if(lastPosition == 0 && position == 0 && convertView != null){
                return convertView;
            }
        }
        lastPosition = position;
        return getViewInner(position, convertView, parent);
    }
    public abstract View getViewInner(int position, View convertView, ViewGroup parent);
}