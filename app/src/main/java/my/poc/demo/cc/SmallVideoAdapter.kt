package my.poc.demo.cc

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.widget.ContentLoadingProgressBar
import com.hm.poc.globe.runtimeContext
import com.hm.poc.globe.toastService
import com.hm.poc.greendao.User
import com.hm.poc.http.data.LiveStreamPullStartResponse
import com.hm.poc.log.ALog
import com.hm.poc.player.IWebRtcPlayerApi
import com.hm.poc.player.SPlayerManager
import com.hm.poc.utils.LiveStreamHelper
import com.unionbroad.app.holder.UserHolder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import my.poc.demo.R


/**
 * @author benzly
 * @date 2024/1/14
 */
data class SmallVideoItem(val user: User) {
    var streamData: LiveStreamPullStartResponse.VideoStreamingData? = null
    var requestJob: Job? = null
    var player: IWebRtcPlayerApi? = null
}

class SmallVideoAdapter(private val context: Context, private val items: List<SmallVideoItem>) : FilterZeroAdapter() {

    companion object {
        const val TAG = "SmallVideoAdapter"
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewInner(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var convertView: View? = convertView
        val holder: ViewHolder
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.cc_item_small_video, null)
            holder = ViewHolder()
            holder.videoContainer = convertView.findViewById(R.id.video_container)
            holder.title = convertView.findViewById(R.id.video_title)
            holder.playIcon = convertView.findViewById(R.id.video_ic)
            holder.loading = convertView.findViewById(R.id.video_loading)
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }
        setData(position, holder, items[position])
        return convertView
    }

    private fun setData(position: Int, holder: ViewHolder, data: SmallVideoItem) {
        //ALog.i(TAG, "setData: $position, $data")
        holder.title?.text = "来源: ${data.user.name}"
        holder.loading?.hide()
        data.requestJob?.cancel()
        if (data.streamData == null) {
            holder.playIcon?.visibility = View.VISIBLE
            holder.playIcon?.setOnClickListener {
                // 请求拉流
                GlobalScope.launch(Dispatchers.Main) {
                    holder.loading?.show()
                    holder.playIcon?.visibility = View.GONE
                    LiveStreamHelper.requestLiveStreamStart(data.user).let {
                        holder?.loading?.hide()
                        ALog.i(TAG, "requestLiveStreamStart: $it")
                        if (it.code == null || it.code != 1 || it.data == null) {
                            holder.playIcon?.visibility = View.VISIBLE
                            toastService.showToast("${data.user.name} 拉流失败")
                        } else {
                            if (it.data!!.whep.isNullOrEmpty()) {
                                toastService.showToast("${data.user.name} 拉流地址缺失whep")
                            } else {
                                data.streamData = it.data
                                playVideo(holder, data)
                            }
                        }
                    }
                }.apply {
                    data.requestJob = this
                }
            }
        } else {
            holder.loading?.hide()
            holder.playIcon?.visibility = View.GONE
            playVideo(holder, data)
        }
    }

    private fun playVideo(holder: ViewHolder, data: SmallVideoItem) {
        toastService.showToast("播放: ${data.streamData?.whep}")
        val player = data.player ?: SPlayerManager.createWebRtcPlayer().apply {
            data.player = this
        }
        holder.videoContainer?.let {
            player.setVideoContainer(it)
        }

        val whepUrl = data.streamData?.whep ?: ""
        ALog.i(TAG, "playVideo: $whepUrl")
        //https://poc.pttlink.com:10443/index/api/whep?app=rtp&stream=34020000001320080003&type=play&code=21041007

        if (true) {
            player.start(whepUrl)
        } else {
            val user = data.user
            val streamNum: String =
                if (UserHolder.isYunTai(user.type) || UserHolder.isExternal(user.type)) user.u_mark4 else user.number.toString()
            SPlayerManager.startPlayer(runtimeContext, whepUrl, true, streamNum, "", false)
        }
    }

    private class ViewHolder {
        var videoContainer: ViewGroup? = null
        var title: TextView? = null
        var playIcon: ImageView? = null
        var loading: ContentLoadingProgressBar? = null
    }

    fun release() {
        // 播放器必须要销毁
        items.forEach {
            it.player?.destroy()
        }
    }
}