package my.poc.demo.cc

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.widget.FrameLayout
import android.widget.GridView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.hm.poc.IPocEngineEventHandler
import com.hm.poc.IPocEngineEventHandler.IncomingInfo
import com.hm.poc.PocEngineFactory
import com.hm.poc.globe.gson
import com.hm.poc.globe.toastService
import com.hm.poc.log.ALog
import com.hm.poc.widget.DevInfoView
import com.hm.poc.widget.VoiceWaveView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import my.poc.demo.R
import java.text.SimpleDateFormat
import java.util.Locale

/**
 * @author benzly
 * @date 2024/1/13
 *
 * 会诊模式：测试语音会议+视频监控结合的业务场景
 * //gb28181
 *
 * blob:https://vv2mzysqu6.feishu.cn/ce614b96-c610-46db-a63c-5404928cfd04
 */
class ConsultationActivity : AppCompatActivity() {

    companion object {
        const val TAG = "ConsultationAcy"

        private const val EXTRA_INCOMING = "extra_incoming"

        /**
         * 显示会诊界面
         */
        fun invoke(context: Context, incomingInfo: IncomingInfo) {
            context.startActivity(Intent(context, ConsultationActivity::class.java).apply {
                putExtra(EXTRA_INCOMING, gson.toJson(incomingInfo))
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            })
        }
    }

    private val pocEngine by lazy { PocEngineFactory.get() }
    private val ccScope = CoroutineScope(Dispatchers.Default)

    private val devInfoView by lazy { findViewById<DevInfoView>(R.id.devInfoView) }
    private val bigWindow by lazy { findViewById<FrameLayout>(R.id.bigWindow) }    // 大窗口视频容器
    private val smallWindow by lazy { findViewById<GridView>(R.id.smallWindow) }        // 小窗口视频容器
    private val meetingTimeTv by lazy { findViewById<TextView>(R.id.meeting_time) }         // 会议通话时长
    private val meetingVoiceView by lazy { findViewById<VoiceWaveView>(R.id.meeting_wave) } // 会议说话声音大小变化
    private val meetingNameTv by lazy { findViewById<TextView>(R.id.meeting_name) }
    private val meetingTimeFormat = SimpleDateFormat("mm:ss", Locale.ENGLISH)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cc)
        if (!PocEngineFactory.hasInit() || !pocEngine.hasServiceConnected()) {
            finish()
            toastService.showToast("服务未链接")
            return
        }
        pocEngine.addEventHandler(pocEventHandler)
        handlerIncomingIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handlerIncomingIntent(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        pocEngine.removeEventHandler(pocEventHandler)
        ccScope.cancel()

        // 确保所有会话断开，会议关闭
        CCHelper.curCcDataModel.audioMeetingSessionId?.let {
            pocEngine.hangUpCall(it)
        }
        CCHelper.curCcDataModel.videoMonitorSessionId?.let {
            pocEngine.hangUpCall(it)
        }
        CCHelper.curCcDataModel.roomId?.let {
            pocEngine.makeAudioMeetingStop(it, null)
        }
        CCHelper.curCcDataModel.clear()

        (smallWindow?.adapter as? SmallVideoAdapter)?.release()
    }

    // 处理语音会议及视频监控
    private fun handlerIncomingIntent(intent: Intent?) {
        if (intent == null) {
            ALog.e(TAG, "handlerIncomingIntent: invalid intent")
            return
        }
        val incomingInfo = gson.fromJson(intent.getStringExtra(EXTRA_INCOMING), IncomingInfo::class.java)
        devInfoView.addDevInfo("来电: $incomingInfo")

        when (incomingInfo.sessionType) {
            IPocEngineEventHandler.SessionType.TYPE_AUDIO_MEETING -> {
                tryAcceptAudioMeeting(incomingInfo)
            }

            IPocEngineEventHandler.SessionType.TYPE_VIDEO_MONITOR_CALL -> {
                tryAcceptVideoMonitor(incomingInfo)
            }

            else -> {
                ALog.e(TAG, "handlerIncomingIntent: invalid session type")
            }
        }
    }

    // 1. 发起语音会议后，收到服务端呼叫，这个业务模式下，应该自动接听
    private fun tryAcceptAudioMeeting(incomingInfo: IncomingInfo) {
        val ret = pocEngine.acceptCall(incomingInfo.sessionId)
        if (!ret) {
            devInfoView.addDevInfo("语音会议接听失败")
            return
        }
        CCHelper.curCcDataModel.audioMeetingSessionId = incomingInfo.sessionId
        CCHelper.curCcDataModel.roomId = incomingInfo.callerId
        devInfoView.addDevInfo("${CCHelper.curCcDataModel}")

        // 接听成功后，显示房间号
        meetingNameTv.text = "会议房间号: ${incomingInfo.callerId}"

        // 显示会议成员参会视频窗口列表
        setupSmallVideoItems()

        // 按需打开轮训查询会议信息，比如后边要踢人了，需要获取成员id才能踢
        // time2UpdateMeetingInfo()
    }

    private fun tryAcceptVideoMonitor(incomingInfo: IncomingInfo) {
        if (CCHelper.curCcDataModel.audioMeetingSessionId == null) {
            devInfoView.addDevInfo("收到视频监控，但语音会议未建立")
            // 正式环境时这里要挂断，现在测试先放过
            //pocEngine.hangUpCall(incomingInfo.sessionId)
            //return
        }
        devInfoView.addDevInfo("收到视频监控呼叫: id=${incomingInfo.sessionId}")

        val config = IPocEngineEventHandler.AcceptCallConfig().apply {
            defaultMute = true // 接听后静麦，因为主要用会议的声音通道
            notHandlerMonitor = true // 接听后不采用内部监控的方式，由业务自己获取renderView去布局
        }
        val ret = pocEngine.acceptCall(incomingInfo.sessionId, config)
        if (!ret) {
            devInfoView.addDevInfo("视频监控接听失败")
            return
        }
        CCHelper.curCcDataModel.videoMonitorSessionId = incomingInfo.sessionId
    }

    /**
     * 主动轮训更新会议信息，目前只能主动查
     */
    private fun time2UpdateMeetingInfo() {
        ccScope.launch {
            while (ccScope.isActive) {
                val roomId = CCHelper.curCcDataModel.roomId
                ALog.i(TAG, "updateMeetingInfo: roomId=$roomId")
                roomId?.let {
                    // 里边是一个xml格式的数据，按需解析，如果不需要则去掉查询逻辑
                    val info = CCHelper.queryMeetingInfo(it)
                }
                delay(1000 * 10)
            }
        }
    }

    // 通话状态事件
    private val pocEventHandler = object : IPocEngineEventHandler() {

        override fun onServiceDisconnected(reason: Int) {
            super.onServiceDisconnected(reason)
            toastService.showToast("服务断开")
            finish()
        }

        override fun onCallConnected(sessionId: Long, remoteId: String?, sessionType: Int) {
            devInfoView.addDevInfo("会话建立: id=$sessionId")
            if (sessionId == CCHelper.curCcDataModel.videoMonitorSessionId) {
                devInfoView.addDevInfo("视频监控已连接")
                setupLocalBigVideo()
            }
        }

        override fun onCallDisconnected(sessionId: Long, reason: Int) {
            devInfoView.addDevInfo("会话断开: id=$sessionId")
        }

        // 更新会议声音分贝变化
        override fun onVoiceDecibelChanged(decibel: Int, isRemote: Boolean) {
            meetingVoiceView.updateVoiceDecibel(decibel)
        }

        // 更新会议时间
        override fun onSessionTimeTick(sesId: Long, time: Long) {
            if (sesId == CCHelper.curCcDataModel.audioMeetingSessionId) {
                meetingTimeTv.text = meetingTimeFormat.format(time * 1000)
            }
        }
    }

    private fun setupLocalBigVideo() {
        ALog.i(TAG, "setupLocalVideo")
        bigWindow.removeAllViews()
        pocEngine.getRendererView(IPocEngineEventHandler.SurfaceType.LOCAL_SURFACE_VIEW)?.let {
            bigWindow.addView(it)
            pocEngine.sendLocalVideo()
            devInfoView.addDevInfo("开始回传视频")
        } ?: kotlin.run {
            ALog.e(TAG, "setupLocalVideo: ignore, get local surface view null")
        }
    }

    private fun setupSmallVideoItems() {
        val items = CCHelper.curCcDataModel.members.map { SmallVideoItem(it) }
        ALog.i(TAG, "setupSmallVideoItems: $items")
        smallWindow.adapter = SmallVideoAdapter(this@ConsultationActivity, items)
        smallWindow.setOnItemClickListener { adapterView, view, i, l ->
            ALog.i(TAG, "onItemClick: $i")
        }
    }

    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        if (event?.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("退出会诊?")
                .setNegativeButton("确定") { p0, p1 -> finish() }.show()
            return true
        }
        return super.dispatchKeyEvent(event)
    }
}